Example of "digital" card game for 2 different iPad or iPhone devices. Game uses Apple's Multipeer Connectivity API for communication between devices.

More about Multipeer Connectivity API from: http://www.appcoda.com/intro-multipeer-connectivity-framework-ios-programming/


![dc.jpg](https://bitbucket.org/repo/aGqra5/images/982859233-dc.jpg)

![iOS Simulator Screen Shot 3.12.2014 1.04.02.png](https://bitbucket.org/repo/aGqra5/images/3466443189-iOS%20Simulator%20Screen%20Shot%203.12.2014%201.04.02.png)