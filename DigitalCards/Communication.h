//
//  Communication.h
//  DigitalCards
//
//  Created by Tero Paananen on 12.11.2014.
//  Copyright (c) 2014 Tero Paananen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MultipeerConnectivity/MultipeerConnectivity.h>

// http://nshipster.com/multipeer-connectivity/
// http://www.appcoda.com/intro-multipeer-connectivity-framework-ios-programming/
// http://stackoverflow.com/questions/23545168/mcnearbyserviceadvertiser-vs-mcadvertiserassistant

@class Card;
@class ViewController;

@protocol CommunicationReseiveDataDelegate <NSObject>
- (void)didReceiveGameStart:(NSArray*)cards;
- (void)didReceiveOpponentReady;
- (void)didReceiveCard:(int)cardId fromPlayerId:(NSString*)fromPlayerId;
- (void)didCancelCard:(int)cardId fromPlayerId:(NSString*)fromPlayerId;
- (void)didReceiveGameEnd:(NSString*)fromPlayerId;
- (void)didReceiveTurn;
- (void)didReceiveDone;
@end


@interface Communication : NSObject <MCSessionDelegate, MCBrowserViewControllerDelegate>

@property (nonatomic, weak) id <CommunicationReseiveDataDelegate> delegate;
@property (nonatomic, strong) NSMutableArray* opponentPlayerIdArray;
@property (nonatomic, strong) NSString* myPlayerId;
@property (nonatomic, assign) BOOL master;
@property (nonatomic, strong) NSString* deviceId;

@property (nonatomic, weak) ViewController* mainViewController;

- (void)startAsMaster;
- (void)starAsSlave;


- (void)sendStartGame:(NSArray*)cards to:(NSString*)playerId;
- (void)sendOpponetReady;
- (void)sendCard:(Card*)card;
- (void)cancelCard:(Card*)card;
- (void)sendYourTurn;
- (void)sendGameEnd;


@end
