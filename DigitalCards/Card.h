//
//  Card.h
//  DigitalCards
//
//  Created by Tero Paananen on 12.11.2014.
//  Copyright (c) 2014 Tero Paananen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Card : NSObject

@property (nonatomic, assign) CGRect rect;

@property (nonatomic, strong) UIImage* faceImage;
@property (nonatomic, strong) UIImage* backImage;

@property (nonatomic, assign) int land;
@property (nonatomic, assign) int value;
@property (nonatomic, assign) BOOL black;

@property (nonatomic, assign) BOOL turned;

@property (nonatomic, assign) CGPoint startMovePoint;
@property (nonatomic, assign) int oldIndex;

@property (nonatomic, strong) NSString* deviceId;


- (id)initWith:(CGRect)rect
         value:(int)value
          land:(int)land
          face:(UIImage*)face
         black:(BOOL)black;


- (void)updatePosTo:(CGPoint)p;

- (void)drawCard;

- (void)cancelMove;

@end
