//
//  ViewController.m
//  DigitalCards
//
//  Created by Tero Paananen on 12.11.2014.
//  Copyright (c) 2014 Tero Paananen. All rights reserved.
//

#import "ViewController.h"
#import "Card.h"
#import "MainView.h"

@interface ViewController ()

@property (nonatomic, strong) NSMutableArray* allCardsArray;
@property (nonatomic, strong) NSMutableArray* cardsArray;
@property (nonatomic, strong) NSMutableArray* targetCardsArray;
@property (nonatomic, strong) MainView* mainView;

@property (nonatomic, strong) UIButton* masterBtn;
@property (nonatomic, strong) UIButton* slaveBtn;
@property (nonatomic, strong) UIButton* turnBtn;

@property (nonatomic, strong) Communication* communication;

@property (nonatomic, strong) UITextView* messageView;

@property (nonatomic, assign) BOOL myTurnn;

@end

@implementation ViewController



- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        self.allCardsArray = [[NSMutableArray alloc]init];
        self.cardsArray = [[NSMutableArray alloc]init];
        self.targetCardsArray = [[NSMutableArray alloc]init];
        self.communication = [[Communication alloc]init];
        self.communication.mainViewController = self;
        self.communication.delegate = self;
    }
    return self;
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationPortrait;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.mainView = [[MainView alloc]initWithFrame:[UIScreen mainScreen].bounds];
    self.mainView.tableDelegate = self;
    self.mainView.clipsToBounds = YES;
    self.mainView.cardsArray = self.cardsArray;
    self.mainView.targetCardsArray = self.targetCardsArray;
    self.view = self.mainView;
    
    int btnHeight = 40;
    int btnWidth = 140;

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        btnHeight = 30;
        btnWidth = 110;
    }
    
    self.masterBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, self.view.bounds.size.height - btnHeight -10, btnWidth, btnHeight)];
    self.masterBtn.autoresizesSubviews = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin;
    self.masterBtn.backgroundColor = [UIColor lightGrayColor];
    [self.masterBtn setTitle:@"I deal" forState:UIControlStateNormal];
    [self.masterBtn addTarget:self action:@selector(masterBtn:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.masterBtn];

    self.slaveBtn = [[UIButton alloc]initWithFrame:CGRectMake(self.masterBtn.bounds.origin.x + self.masterBtn.bounds.size.width + 20, self.view.bounds.size.height - btnHeight-10, btnWidth, btnHeight)];
    self.slaveBtn.autoresizesSubviews = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin;
    self.slaveBtn.backgroundColor = [UIColor lightGrayColor];
    [self.slaveBtn setTitle:@"I want cards" forState:UIControlStateNormal];
    [self.slaveBtn addTarget:self action:@selector(slaveBtn:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.slaveBtn];

    self.messageView = [[UITextView alloc]initWithFrame:CGRectZero];
    self.messageView.autoresizesSubviews = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin;
    self.messageView.editable = NO;
    self.messageView.textAlignment = NSTextAlignmentRight;
    self.messageView.text = @"Select 'I deal' or 'I want cards'";
    self.messageView.font = [UIFont fontWithName:@"Helvetica" size:20];
    self.messageView.backgroundColor = [UIColor colorWithWhite:0.9f alpha:0.7f];

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [self.view addSubview:self.messageView];
    }

    
    self.turnBtn = [[UIButton alloc]initWithFrame:CGRectMake(self.view.bounds.size.width - btnWidth-10,self.view.bounds.size.height * 0.25f + 5, btnWidth, btnHeight)];
    self.turnBtn.backgroundColor = [UIColor lightGrayColor];
    self.turnBtn.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin;
    [self.turnBtn setTitle:@"I am done!" forState:UIControlStateNormal];
    [self.turnBtn addTarget:self action:@selector(turnBtnPress) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.turnBtn];

    UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tap:)];
    tap.numberOfTapsRequired = 2;
    [self.mainView addGestureRecognizer:tap];
    
    [self.mainView setNeedsDisplay];
}

- (void)viewWillLayoutSubviews
{
    int w = self.view.bounds.size.width*0.55f;
    self.messageView.frame = CGRectMake(self.view.bounds.size.width - (w+10), self.view.bounds.size.height - 50, w, 40);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showInfo:(NSString*)info animate:(BOOL)animate
{
    [[NSOperationQueue mainQueue]addOperationWithBlock:^{
        self.messageView.text = info;
        
        if (animate) {
            [self performSelector:@selector(clearInfo) withObject:nil afterDelay:5];
        }
    }];

}

- (void)clearInfo
{
    self.messageView.text = @"";
}

- (void)myTurn:(BOOL)turn
{
    self.myTurnn = turn;
    self.turnBtn.hidden = !self.myTurnn;
    
    if (turn) {
        [self showInfo:@"My turn" animate:NO];
    } else {
        [self showInfo:@"Waiting opponent turn" animate:NO];
    }
}

- (void)turnBtnPress
{
    [self myTurn:NO];
    
    [self.communication sendYourTurn];
}

- (void)masterBtn:(id)sender
{
    [self myTurn:YES];
    [self showInfo:@"I deal cards. Waiting opponents..." animate:NO];
    
    [self createAllCards];
    [self.communication startAsMaster];
    
    self.masterBtn.hidden = YES;
    self.slaveBtn.hidden = YES;
    
    [self.view setNeedsDisplay];
}

- (void)slaveBtn:(id)sender
{
    [self myTurn:NO];
    [self showInfo:@"I am waiting cards from dealer..." animate:NO];

    [self.communication starAsSlave];

    self.masterBtn.hidden = YES;
    self.slaveBtn.hidden = YES;

    [self.view setNeedsDisplay];
}

- (void)tap:(UIGestureRecognizer*)g
{
    [self positionMyCards];
    [self.view setNeedsDisplay];
}

- (Card*)createCard:(int)value
{
    Card* c = nil;

    CGSize s = [self cardSize];
    
    if (value > 1000 && value < 2000) {
        c = [[Card alloc]initWith:CGRectMake(0, 0, s.width, s.height)
                                  value:value
                                   land:1
                                   face:[UIImage imageNamed:[NSString stringWithFormat:@"club%d.png",value-1000]]
                                  black:YES];
    }
    else if (value > 2000 && value < 3000) {
        c = [[Card alloc]initWith:CGRectMake(0, 0, s.width, s.height)
                            value:value
                             land:2
                             face:[UIImage imageNamed:[NSString stringWithFormat:@"diamond%d.png",value-2000]]
                            black:NO];
    }
    else if (value > 3000 && value < 4000) {
        c = [[Card alloc]initWith:CGRectMake(0, 0, s.width, s.height)
                            value:value
                             land:3
                             face:[UIImage imageNamed:[NSString stringWithFormat:@"spade%d.png",value-3000]]
                            black:YES];
    }
    else if (value > 4000 && value < 5000) {
        c = [[Card alloc]initWith:CGRectMake(0, 0, s.width, s.height)
                            value:value
                             land:4
                             face:[UIImage imageNamed:[NSString stringWithFormat:@"heart%d.png",value-4000]]
                            black:NO];
    }
    return c;
}

- (CGSize)cardSize
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        return CGSizeMake(75, 110);
    } else {
        return CGSizeMake(75*0.5f, 110*0.5f);
    }
}

-(Card*)getRandomCardFrom:(NSMutableArray*)array
{
    Card* ret;
    int r = -1;
    
    NSUInteger max = ([array count]-1);
    if (max>0)
        r = arc4random() % max;
    
    if(r<0)
        r = 0;
    
    ret = [array objectAtIndex:r];
    [array removeObject:ret];
    
    return ret;
}

- (void)createAllCards
{
    for (int i=1;i<=13;i++) {
        Card* c = [self createCard:i+1000];
        [self.allCardsArray addObject:c];
    }
    
    for (int i=1;i<=13;i++) {
        Card* c = [self createCard:i+2000];
        [self.allCardsArray addObject:c];
    }
    
    for (int i=1;i<=13;i++) {
        Card* c = [self createCard:i+3000];
        [self.allCardsArray addObject:c];
    }
    
    for (int i=1;i<=13;i++) {
        Card* c = [self createCard:i+4000];
        [self.allCardsArray addObject:c];
    }
    
    // TODO: for testing
    //[self sendCardsToOpponents];
    //[self positionMyCards];
    
}

- (void)positionMyCards
{
    CGSize s = [self cardSize];
    int index = 0;
    int y = self.view.bounds.size.height * 0.4f;
    int xcap = 5;
    int cardsInRow = 0;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        cardsInRow = self.view.bounds.size.width / (s.width+xcap*2.0f);
    } else {
        xcap = 20;
        cardsInRow = self.view.bounds.size.width / (s.width+xcap*2.0f);
    }
    int x = xcap*3;
    
    for (Card* c in self.cardsArray) {
        c.rect = CGRectMake(x, y, s.width, s.height);
        c.deviceId = self.mainView.deviceId;
        x += s.width+xcap;
        index++;
   
        if (index > cardsInRow) {
            y += s.height * 1.2f;
            x = xcap*3;
            index = 0;
        }
    }
    
}


- (void)sendCardsToOpponents
{
    NSMutableArray* cardsForSend = [[NSMutableArray alloc]initWithCapacity:[self.allCardsArray count]];
    NSMutableArray* removeCards = [[NSMutableArray alloc]initWithCapacity:[self.allCardsArray count]];
    
    int cardCountToSend = [self.allCardsArray count] * 0.5f;
    
    for (int i=0; i<cardCountToSend; i++) {
        Card* c = [self getRandomCardFrom:self.allCardsArray];
        [removeCards addObject:c];
        [cardsForSend addObject:c];
    }

    [self.allCardsArray removeObjectsInArray:removeCards];
    [self.cardsArray addObjectsFromArray:self.allCardsArray];
    [self.allCardsArray removeAllObjects];
    
    [self.targetCardsArray removeAllObjects];
 
    [self.communication sendStartGame:cardsForSend to:@"xyz"];
    
    [self positionMyCards];
}

#pragma mark - MainViewDelegate

- (void)cardMovedToTarget:(Card*)card
{
    if (self.myTurnn) {
        
        if ([self.targetCardsArray containsObject:card] == NO) {
            if ([self.targetCardsArray count] > 4) {
                [self.targetCardsArray removeObjectAtIndex:0];
            }
            
            [self.targetCardsArray addObject:card];
            [self.cardsArray removeObject:card];
            [self updateTargetCardsPos:card];
            [self sendCard:card];

        } else {
            [card cancelMove];
        }
        
    } else {
        [card cancelMove];
    }

    [self.view setNeedsDisplay];
}

- (void)cardMovedFromTarget:(Card*)card
{
    if (self.myTurnn) {
        if ([self.cardsArray containsObject:card] == NO) {
            [self.cardsArray addObject:card];
        } else {
            [self.cardsArray removeObjectAtIndex:[self.cardsArray count]-1];
            [self.cardsArray insertObject:card atIndex:card.oldIndex];
        }
        
        [self.targetCardsArray removeObject:card];
        
        [self.communication cancelCard:card];
        
        [self positionMyCards];

    } else {
        [card cancelMove];
    }
    
    [self.view setNeedsDisplay];
}


- (void)updateTargetCardsPos:(Card*)newCard
{
    NSUInteger cards = [self.targetCardsArray count] * 40 + 75;
    int x = (self.view.bounds.size.width - cards) * 0.5f;
    
    for (Card* card in self.targetCardsArray) {
        card.rect = CGRectMake(x+=40, 50, card.rect.size.width, card.rect.size.height);
    }
}


- (void)sendCard:(Card*)card
{
    if (self.myTurnn) {
        [self showInfo:@"Card sent, any more?" animate:YES];
        [self.communication sendCard:card];
    }
}

#pragma mark - CommunicationReseiveDataDelegate

- (void)didReceiveGameStart:(NSArray*)cards
{
    [self myTurn:NO];
    
    [self.cardsArray removeAllObjects];
   
    for (NSNumber* cardId in cards) {
        [self.cardsArray addObject:[self createCard:[cardId intValue]]];
    }
    
    [self positionMyCards];
    
    [[NSOperationQueue mainQueue]addOperationWithBlock:^{
        [self showInfo:@"Opponent ready!" animate:NO];
        [self.view setNeedsDisplay];
    }];

    [self.communication sendOpponetReady];
}

- (void)didReceiveTurn
{
    [[NSOperationQueue mainQueue]addOperationWithBlock:^{
        [self myTurn:YES];
    }];
}

- (void)didReceiveOpponentReady
{
    [[NSOperationQueue mainQueue]addOperationWithBlock:^{
        [self showInfo:@"Opponent ready!" animate:NO];
        [self.view setNeedsDisplay];
    }];
}

- (void)didReceiveDone
{
    [self sendCardsToOpponents];
    
    [[NSOperationQueue mainQueue]addOperationWithBlock:^{
        [self.view setNeedsDisplay];
    }];

}


- (void)didReceiveCard:(int)cardId fromPlayerId:(NSString*)fromPlayerId
{
    //NSPredicate *predicate = [NSPredicate predicateWithFormat:@"value == %@", [NSNumber numberWithInt:cardId]];
    //NSArray *filteredArray = [self.cardsArray filteredArrayUsingPredicate:predicate];

    if (cardId) {
        Card* card = [self createCard:cardId];
        card.deviceId = fromPlayerId;
        if (card) {
            
            if ([self.targetCardsArray count] > 4) {
                [self.targetCardsArray removeObjectAtIndex:0];
            }
            
            [self.targetCardsArray addObject:card];
            [self updateTargetCardsPos:card];
        }
    }
    
    [[NSOperationQueue mainQueue]addOperationWithBlock:^{
        [self showInfo:@"Card received." animate:YES];
        [self.view setNeedsDisplay];
    }];
    
}

- (void)didCancelCard:(int)cardId fromPlayerId:(NSString*)fromPlayerId
{
    if (cardId) {
        Card* card = [self createCard:cardId];
        card.deviceId = fromPlayerId;

        if (card) {
            for (int i=0; i<[self.targetCardsArray count]; i++) {
                Card* c = [self.targetCardsArray objectAtIndex:i];
                if (c.value == card.value) {
                    [self.targetCardsArray removeObjectAtIndex:i];
                    break;
                }
            }
        }
    }
    
    [[NSOperationQueue mainQueue]addOperationWithBlock:^{
        [self showInfo:@"Card cancelled." animate:YES];
        [self.view setNeedsDisplay];
    }];

}


- (void)didReceiveGameEnd:(NSString*)fromPlayerId
{
    [self.targetCardsArray removeAllObjects];
    [self.cardsArray removeAllObjects];

    [[NSOperationQueue mainQueue]addOperationWithBlock:^{
        [self showInfo:@"Game end!" animate:NO];
        [self.view setNeedsDisplay];
    }];
}


@end
