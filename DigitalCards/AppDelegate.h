//
//  AppDelegate.h
//  DigitalCards
//
//  Created by Tero Paananen on 12.11.2014.
//  Copyright (c) 2014 Tero Paananen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

