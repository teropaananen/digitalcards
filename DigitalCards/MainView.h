//
//  MainView.h
//  DigitalCards
//
//  Created by Tero Paananen on 12.11.2014.
//  Copyright (c) 2014 Tero Paananen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Card.h"

@protocol MainViewDelegate <NSObject>

- (void)cardMovedToTarget:(Card*)card;
- (void)cardMovedFromTarget:(Card*)card;

@end


@interface MainView : UIView

@property (nonatomic, weak) id<MainViewDelegate> tableDelegate;
@property (nonatomic, weak) NSMutableArray* targetCardsArray;
@property (nonatomic, weak) NSMutableArray* cardsArray;
@property (nonatomic, weak) Card* activeCard;
@property (nonatomic, strong) NSString* deviceId;

@end
