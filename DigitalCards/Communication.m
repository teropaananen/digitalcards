//
//  Communication.m
//  DigitalCards
//
//  Created by Tero Paananen on 12.11.2014.
//  Copyright (c) 2014 Tero Paananen. All rights reserved.
//

#import "Communication.h"
#import "Card.h"
#import "ViewController.h"

static NSString* const DigitalCardsServiceType = @"tepaanan-cards";

@interface Communication ()

@property (nonatomic, strong) MCPeerID* localPeerID;
@property (nonatomic, strong) MCAdvertiserAssistant* advertiser;
@property (nonatomic, strong) MCBrowserViewController* serviceBrowser;
@property (nonatomic, strong) MCSession* session;

@property (nonatomic, assign) BOOL oneConnected;

@end

@implementation Communication

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.deviceId = [[UIDevice currentDevice]identifierForVendor].UUIDString;
        self.localPeerID = [[MCPeerID alloc] initWithDisplayName:[[UIDevice currentDevice] name]];
    }
    return self;
}


- (void)setUpMultipeer
{
    self.oneConnected = NO;

    //  Setup session
    self.session = [[MCSession alloc] initWithPeer:self.localPeerID];
    self.session.delegate = self;
    
    //  Setup BrowserViewController
    self.serviceBrowser = [[MCBrowserViewController alloc] initWithServiceType:DigitalCardsServiceType session:self.session];
    self.serviceBrowser.delegate = self;
    
    //  Setup Advertiser
    self.advertiser = [[MCAdvertiserAssistant alloc] initWithServiceType:DigitalCardsServiceType discoveryInfo:nil session:self.session];
    [self.advertiser start];
}

- (void)startAsMaster
{
    NSLog(@"MASTER");
    self.master = YES;
    [self setUpMultipeer];
    [self.mainViewController presentViewController:self.serviceBrowser animated:YES completion:nil];
}

- (void)starAsSlave
{
    NSLog(@"SLAVE");
    self.master = NO;
    [self setUpMultipeer];
}

#pragma mark - Send

- (void)sendStartGame:(NSArray*)cards to:(NSString*)playerId
{
    NSMutableArray* cardIdArray = [[NSMutableArray alloc]initWithCapacity:[cards count]];
    for (Card* card in cards) {
        [cardIdArray addObject:[NSNumber numberWithInt:card.value]];
    }
    
    NSDictionary* jsonDict = @{@"ACTION":@"START",
                               @"FROM_DEVICE":self.deviceId,
                               @"TO_PLAYER":playerId,
                               @"cards":cardIdArray
                               };
    
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:jsonDict options:NSJSONWritingPrettyPrinted error:nil];
    //NSString* jsonString = [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];

    //NSLog(@"%@",jsonString);

    [self doSend:jsonData];
}

- (void)sendCard:(Card*)card
{
    NSDictionary* jsonDict = @{@"ACTION":@"CARD",
                               @"FROM_DEVICE":self.deviceId,
                               @"cardid":[NSNumber numberWithInt:card.value]
                               };
    
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:jsonDict options:NSJSONWritingPrettyPrinted error:nil];
    //NSString* jsonString = [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];
    
    //NSLog(@"%@",jsonString);

    [self doSend:jsonData];
}

- (void)cancelCard:(Card*)card
{
    NSDictionary* jsonDict = @{@"ACTION":@"CANCEL-CARD",
                               @"FROM_DEVICE":self.deviceId,
                               @"cardid":[NSNumber numberWithInt:card.value]
                               };
    
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:jsonDict options:NSJSONWritingPrettyPrinted error:nil];
    //NSString* jsonString = [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];
    
    //NSLog(@"%@",jsonString);
    
    [self doSend:jsonData];
}

- (void)doSend:(NSData*)jsonData
{
    NSError *error = nil;
    [self.session sendData:jsonData
                   toPeers:[self.session connectedPeers]
                  withMode:MCSessionSendDataReliable
                     error:&error];
    

    if (error) {
        NSLog(@"%@",error.description);
    }
}

- (void)sendOpponetReady
{
    NSDictionary* jsonDict = @{@"ACTION":@"READY",
                               @"FROM_DEVICE":self.deviceId,
                               };
    
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:jsonDict options:NSJSONWritingPrettyPrinted error:nil];
    //    NSString* jsonString = [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];
    
    //NSLog(@"%@",jsonString);
    
    [self doSend:jsonData];
    
}


- (void)sendYourTurn
{
    NSDictionary* jsonDict = @{@"ACTION":@"TURN",
                               @"FROM_DEVICE":self.deviceId,
                               };
    
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:jsonDict options:NSJSONWritingPrettyPrinted error:nil];
//    NSString* jsonString = [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];
    
    //NSLog(@"%@",jsonString);
    
    [self doSend:jsonData];
}


- (void)sendGameEnd
{
    NSDictionary* jsonDict = @{@"ACTION":@"STOP",
                               @"FROM_DEVICE":self.deviceId
                               };
    
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:jsonDict options:NSJSONWritingPrettyPrinted error:nil];
    //NSString* jsonString = [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];
    
    //NSLog(@"%@",jsonString);
    
    [self doSend:jsonData];
}


#pragma mark - Receive

- (void)handleReceivedData:(NSData*)data
{
    NSError* err = nil;
    NSDictionary* dataDic = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&err];
    
    if (!err) {
        NSLog(@"%@",dataDic);
        
        NSString* fromDevice = [dataDic objectForKey:@"FROM_DEVICE"];
        if ([fromDevice isEqualToString:self.deviceId] == NO) {
            
            NSString* action = [dataDic objectForKey:@"ACTION"];
            if ([action isEqualToString:@"START"]) {
                [self.delegate didReceiveGameStart:[dataDic objectForKey:@"cards"]];
                
            } else if ([action isEqualToString:@"STOP"]) {
                [self.delegate didReceiveGameEnd:[dataDic objectForKey:@"FROM_DEVICE"]];
                
            } else if ([action isEqualToString:@"CARD"]) {
                [self.delegate didReceiveCard:[[dataDic objectForKey:@"cardid"]intValue] fromPlayerId:[dataDic objectForKey:@"FROM_DEVICE"]];

            } else if ([action isEqualToString:@"TURN"]) {
                [self.delegate didReceiveTurn];

            } else if ([action isEqualToString:@"READY"]) {
                [self.delegate didReceiveOpponentReady];

            } else if ([action isEqualToString:@"CANCEL-CARD"]) {
                [self.delegate didCancelCard:[[dataDic objectForKey:@"cardid"]intValue] fromPlayerId:[dataDic objectForKey:@"FROM_DEVICE"]];
            }
            
        } else {
            NSLog(@"was my own message?");
        }
    }
}

// TODO: CommunicationReseiveDataDelegate


#pragma mark - MCSessionDelegate

- (void)session:(MCSession *)session peer:(MCPeerID *)peerID didChangeState:(MCSessionState)state
{
    /*
    if (state == MCSessionStateConnected) {
        if (self.oneConnected){
            [session disconnect];
        } else {
            self.oneConnected = YES;
        }
    }
    */ 
}

// Received data from remote peer
- (void)session:(MCSession *)session didReceiveData:(NSData *)data fromPeer:(MCPeerID *)peerID
{
    if ([peerID.displayName isEqualToString:self.localPeerID.displayName] == NO) {
        [self handleReceivedData:data];
    } else {
        NSLog(@"my message?");
    }
}

// Received a byte stream from remote peer
- (void)session:(MCSession *)session didReceiveStream:(NSInputStream *)stream withName:(NSString *)streamName fromPeer:(MCPeerID *)peerID
{
}

// Start receiving a resource from remote peer
- (void)session:(MCSession *)session didStartReceivingResourceWithName:(NSString *)resourceName fromPeer:(MCPeerID *)peerID withProgress:(NSProgress *)progress
{
}

// Finished receiving a resource from remote peer and saved the content in a temporary location - the app is responsible for moving the file to a permanent location within its sandbox
- (void)session:(MCSession *)session didFinishReceivingResourceWithName:(NSString *)resourceName fromPeer:(MCPeerID *)peerID atURL:(NSURL *)localURL withError:(NSError *)error
{
    NSLog(@"%@",error.description);
}



#pragma mark - MCBrowserViewControllerDelegate

// Notifies the delegate, when the user taps the done button
- (void)browserViewControllerDidFinish:(MCBrowserViewController *)browserViewController
{
    [self.serviceBrowser dismissViewControllerAnimated:YES completion:nil];
    [self.delegate didReceiveDone];
}

// Notifies delegate that the user taps the cancel button.
- (void)browserViewControllerWasCancelled:(MCBrowserViewController *)browserViewController
{
    [self.serviceBrowser dismissViewControllerAnimated:YES completion:nil];
}






@end
