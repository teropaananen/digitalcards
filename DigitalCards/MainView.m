//
//  MainView.m
//  DigitalCards
//
//  Created by Tero Paananen on 12.11.2014.
//  Copyright (c) 2014 Tero Paananen. All rights reserved.
//

#import "MainView.h"
#import "Card.h"

@interface MainView ()

@property (nonatomic, assign) CGPoint prevPoint;
@property (nonatomic, assign) float xCap;
@property (nonatomic, assign) float yCap;
@property (nonatomic, assign) CGRect targetRect;
@property (nonatomic, assign) CGRect sourceRect;

@property (nonatomic, strong) UILabel* targetLabel;
@property (nonatomic, strong) UILabel* sourceLabel;

@end


@implementation MainView

#define RGB(r, g, b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        self.deviceId = [[UIDevice currentDevice]identifierForVendor].UUIDString;

        UIFont* f = [UIFont fontWithName:@"Hevletica" size:[UIFont smallSystemFontSize]];

        self.targetLabel = [[UILabel alloc]init];
        self.targetLabel.text = @"Move your card to here and your opponent will receive the card.";
        self.targetLabel.textColor = [UIColor whiteColor];
        self.targetLabel.backgroundColor = [UIColor clearColor];
        self.targetLabel.userInteractionEnabled = NO;
        self.targetLabel.alpha = 0.5f;
        self.targetLabel.font = f;
        self.targetLabel.numberOfLines = 2;
        [self addSubview:self.targetLabel];

        self.sourceLabel = [[UILabel alloc]init];
        self.sourceLabel.text = @"Your cards.";
        self.sourceLabel.textColor = [UIColor whiteColor];
        self.sourceLabel.backgroundColor = [UIColor clearColor];
        self.sourceLabel.userInteractionEnabled = NO;
        self.sourceLabel.font = f;
        self.sourceLabel.alpha = 0.5f;
        
        
        [self addSubview:self.sourceLabel];
    
    }
    return self;
}

- (void)layoutSubviews
{
    self.targetRect = CGRectMake(0,
                                 0,
                                 self.bounds.size.width,
                                 self.bounds.size.height * 0.25f);
    
    self.sourceRect = CGRectMake(0,
                                 self.bounds.size.height * 0.25f,
                                 self.bounds.size.width,
                                 self.bounds.size.height * 0.75f);
    
    self.targetLabel.frame = CGRectMake(10, self.bounds.size.height * 0.25f - 60, self.bounds.size.width-20, 60);
    self.sourceLabel.frame = CGRectMake(10, self.bounds.size.height * 0.25f + 10, self.bounds.size.width-20, 20);
}



-(void)touchesBegan:(NSSet*)touches withEvent:(UIEvent*)event
{
    UITouch* touchPoint = [touches anyObject];
    CGPoint point = [touchPoint locationInView:self];
    self.activeCard = [self findCard:point];

    self.xCap = point.x - _activeCard.rect.origin.x;
    self.yCap = point.y - _activeCard.rect.origin.y;
    _prevPoint = point;

    self.activeCard.startMovePoint = _activeCard.rect.origin;
    
    [self setNeedsDisplay];
}

- (Card*)findCard:(CGPoint)point
{
    Card* ret = nil;
    
    int index = -1;
    for (int i = [self.cardsArray count]-1 ; i>-1 ; i--) {
        Card* card = [self.cardsArray objectAtIndex:i];
        if (CGRectContainsPoint(card.rect,point)) {
            ret = card;
            index = i;
            break;
        }
    }
    
    if (ret) {
        ret.oldIndex = index;
        [self.cardsArray removeObjectAtIndex:index];
        [self.cardsArray insertObject:ret atIndex:[self.cardsArray count]];

    } else {
        Card* card = [self.targetCardsArray lastObject];
        if (CGRectContainsPoint(card.rect,point)) {
            if ([card.deviceId isEqualToString:self.deviceId]) {
                ret = card;
            }
        }
    }
    
    return ret;
}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (_activeCard) {
        UITouch* touch = [touches anyObject];
        CGPoint point = [touch locationInView:self];
        [_activeCard updatePosTo:CGPointMake(point.x - self.xCap, point.y - self.yCap)];

        // Optimize drawing with clipping when user moves cards
        int x = ABS(_prevPoint.x - point.x) + 15;
        int y = ABS(_prevPoint.y - point.y) + 15;
        _prevPoint = point;
        CGRect rect;
        rect = _activeCard.rect;
        rect.origin.x -= x;
        rect.origin.y -= y;
        rect.size.width += x*2;
        rect.size.height = self.bounds.size.height * 0.5;
        [self setNeedsDisplayInRect:rect];
    }
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (_activeCard) {
        UITouch* touch = [touches anyObject];
        CGPoint point = [touch locationInView:self];
        [_activeCard updatePosTo:CGPointMake(point.x - self.xCap, point.y - self.yCap)];

        
        // Card moved to target?
        if (CGRectContainsPoint(self.targetRect,
                                CGPointMake(_activeCard.rect.origin.x + _activeCard.rect.size.width * 0.5f, _activeCard.rect.origin.y + _activeCard.rect.size.height * 0.5f))) {
            if (self.tableDelegate) {
                [self.tableDelegate cardMovedToTarget:_activeCard];
            }
        
        // Card moved from target to cards
        } else if (CGRectContainsPoint(self.sourceRect,
                                       CGPointMake(_activeCard.rect.origin.x + _activeCard.rect.size.width * 0.5f, _activeCard.rect.origin.y + _activeCard.rect.size.height * 0.5f))) {
            
            if ([self.cardsArray containsObject:_activeCard]) {
                [_activeCard cancelMove];
                
            } else {
                if (self.tableDelegate) {
                    [self.tableDelegate cardMovedFromTarget:_activeCard];
                }
            }
        }
        
        [self setNeedsDisplay];
        _activeCard = nil;
    }
}


- (void)drawRect:(CGRect)rect
{
    // Top
    [RGB(0, 80, 0) set];
    UIRectFill(self.targetRect);

    // Draw bottom main green color
    [RGB(0, 100, 0) set];
    UIRectFill(self.sourceRect);

    for (Card* card in self.targetCardsArray) {
        [card drawCard];
    }
    for (Card* card in self.cardsArray) {
        [card drawCard];
    }
    
    if (self.activeCard) {
        [self.activeCard drawCard];
    }
}


@end
