//
//  Card.m
//  DigitalCards
//
//  Created by Tero Paananen on 12.11.2014.
//  Copyright (c) 2014 Tero Paananen. All rights reserved.
//

#import "Card.h"

@implementation Card

- (id)initWith:(CGRect)rect
         value:(int)value
          land:(int)land
          face:(UIImage*)face
         black:(BOOL)black
{
    self = [super init];
    if (self) {
        self.value = value;
        self.land = land;
        self.faceImage = face;
        self.black = black;
        self.rect = rect;
        self.turned = YES;
    }
    return self;
}

- (void)updatePosTo:(CGPoint)p
{
    CGRect newRect = self.rect;
    CGPoint point;
    point.x = p.x;
    point.y = p.y;
    newRect.origin = point;
    self.rect = newRect;
}

- (void)drawCard
{
    if(self.turned){
        [self.faceImage drawInRect:self.rect];
    }
    else {
        [self.backImage drawInRect:self.rect];
    }
}

- (void)cancelMove
{
    self.rect = CGRectMake(self.startMovePoint.x, self.startMovePoint.y, self.rect.size.width, self.rect.size.height);
}


@end
