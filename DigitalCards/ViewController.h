//
//  ViewController.h
//  DigitalCards
//
//  Created by Tero Paananen on 12.11.2014.
//  Copyright (c) 2014 Tero Paananen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainView.h"
#import "Communication.h"

@interface ViewController : UIViewController <MainViewDelegate, CommunicationReseiveDataDelegate>


@end

